package com.payroll.businessLogic;

import java.util.Date;

/**
 * Time Card
 */
public class TimeCard {
    public static final double OVERTIME_THRESHOLD = 8.0;
    private BusinessCalendar businessCalendar = new TaiwanBusinessCalendar();

    public String name;
    private final Date checkinDate;
    private final Date checkoutDate;
    private int baseHours;
    private int overTimeHours;

    public void setBusinessCalendar(BusinessCalendar bCalendar) {
        this.businessCalendar = bCalendar;
    }

    public TimeCard(String checkinDateString, String checkoutDateString) {
        this("Unknown", checkinDateString, checkoutDateString);
    }

    public TimeCard(String name, String checkinDateString, String checkoutDateString) {
        this(name, TimeUtils.buildDate(checkinDateString), TimeUtils.buildDate(checkoutDateString));
    }

    public TimeCard(String name, Date checkinDate, Date checkoutDate) {
        this.name = name;
        this.checkinDate = checkinDate;
        this.checkoutDate = checkoutDate;
    }

    public TimeCard(Date checkinDate, Date checkoutDate) {
        this.name = name;
        this.checkinDate = checkinDate;
        this.checkoutDate = checkoutDate;
    }

    public Date getCheckinDate() {
        return (Date) this.checkinDate.clone();
    }

    public Date getCheckoutDate() {
        return (Date) this.checkoutDate.clone();
    }

    /**
     * Calculate base hours and over-time hours
     */
    public void process() {
        if (!TimeUtils.isDateEarlierThan(this.checkinDate, this.checkoutDate))
            throw new IllegalArgumentException("'check in date can not great or equal to 'check out date'");
        long totalTimeInMs = TimeUtils.compareDate(this.checkinDate, this.checkoutDate);

        // lunch break
        if (TimeUtils.isTimeBefore(this.checkinDate, 12) && TimeUtils.isTimeAfter(this.checkoutDate, 13)) {
            totalTimeInMs = totalTimeInMs - (3600 * 1000);
        }

        // calculate base hours / over-time hour
        int totalHours = (int) (totalTimeInMs / 1000 / 3600);
        this.baseHours = totalHours;
        this.overTimeHours = 0;
    }

    public int getBaseHours() {
        return baseHours;
    }

    public void setBaseHours(int baseHours) {
        this.baseHours = baseHours;
    }

    public int getOverTimeHours() {
        return overTimeHours;
    }

    public void setOverTimeHours(int oTHours) {
        overTimeHours = oTHours;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
