<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="header.jsp" />

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <jsp:include page="menu.jsp">
        <jsp:param name="item" value="timeCard" />
      </jsp:include>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
      </div>

      <h2>Time Cards</h2>
      <div class="card">
        <div class="card-body">

          <c:out value="${employee}"/>
          <c:out value="${checkInTime}"/>
          <c:out value="${checkOutTime}"/>

          <c:if test="${isSuccess == true}">
            <div class="alert alert-primary" role="alert">
              Time card added successfully.
            </div>
          </c:if>

          <c:if test="${isError == true}">
          <div class="alert alert-danger" role="alert">
              Something went wrong! <c:out value="${errorCause}" />
          </div>
          </c:if>

          <form method="POST" action="timeCard">
            <div class="form-group row">
              <label for="employee" class="col-sm-2 col-form-label">Employee:</label>
              <div class="col-sm-3">
                <select class="form-control" id="employee" name="employee">
                  <option value="">--- Choose ---</option>
                  <option value="Bob">Bob</option>
                  <option value="John">John</option>
                  <option value="Mary">Mary</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="checkInDate" class="col-sm-2 col-form-label">Check-In Time:</label>
              <div class="col-sm-3">
                <input id="checkInDate" name="checkInDate" type="text" class="form-control" placeholder="yyyy/MM/dd">
              </div>

              <div class="col-sm-3">
                <select class="form-control" id="checkInHour" name="checkInHour">
                  <option value="">--- Choose ---</option>
                  <option value="01">1</option>
                  <option value="02">2</option>
                  <option value="03">3</option>
                  <option value="04">4</option>
                  <option value="05">5</option>
                  <option value="06">6</option>
                  <option value="07">7</option>
                  <option value="08">8</option>
                  <option value="09">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="checkOutDate" class="col-sm-2 col-form-label">Check-Out Time:</label>
              <div class="col-sm-3">
                <input id="checkOutDate" name="checkOutDate" type="text" class="form-control" placeholder="yyyy/MM/dd">
              </div>

              <div class="col-sm-3">
                <select class="form-control" id="checkOutHour" name="checkOutHour">
                  <option value="">--- Choose ---</option>
                  <option value="01">1</option>
                  <option value="02">2</option>
                  <option value="03">3</option>
                  <option value="04">4</option>
                  <option value="05">5</option>
                  <option value="06">6</option>
                  <option value="07">7</option>
                  <option value="08">8</option>
                  <option value="09">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                </select>
              </div>
            </div>

            <button type="submit" class="btn btn-primary mb-2">Submit</button>
          </form>

        </div>
      </div>

      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th>#</th>
              <th>Employee Name</th>
              <th>Check-In Time</th>
              <th>Check-Out Time</th>
              <th>Salary</th>
            </tr>
          </thead>
          <tbody>
          <c:forEach var="timeCard" items="${timeCards}" varStatus="loop">
            <tr>
            <td>${loop.index + 1}.</td>
            <td>${timeCard.name}</td>
            <td><fmt:formatDate value="${timeCard.checkinDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
            <td><fmt:formatDate value="${timeCard.checkoutDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
            <td>200</td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
    </main>
  </div>
</div>

<jsp:include page="footer.jsp" />

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../../assets/js/vendor/popper.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
  feather.replace()
</script>
</body>
</html>
